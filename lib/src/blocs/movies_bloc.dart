import '../resources/repository.dart';
import 'package:rxdart/rxdart.dart';
import '../models/movie_model.dart';

class MoviesBloc {
  final _repository = Repository();
  final _moviesFetcher = PublishSubject<MovieModel>();
  final _popularMoviesFetcher = PublishSubject<MovieModel>();
  int pageNumber = 0;

  int yearDropDown = null;
  String genreDropDown = null;

  Observable<MovieModel> get allMovies => _moviesFetcher.stream;
  Observable<MovieModel> get popularMovies => _popularMoviesFetcher.stream;

  getMovies() async {
    MovieModel movieModel = await _repository.getMovies();
    _moviesFetcher.sink.add(movieModel);
  }

  getPopularMovies() async {
    MovieModel movieModel = await _repository.getPopularMovies(1);
    _popularMoviesFetcher.sink.add(movieModel);
  }

  getPopularPageCounter() {
    return pageNumber;
  }

  setPopularPageCounter(int page){
    _repository.setPopularPageCounter(page);
  }

  getMoviesWithYear() {
    _repository.getMoviesWithYear(yearDropDown);
  }
  getMoviesWithGenre() {
    _repository.getMoviesWithGenre(genreDropDown);
  }
  getMoviesWithYearAndGenre() {
    _repository.getMoviesWithYearAndGenre(yearDropDown, genreDropDown);
  }

  Future <List<Movie>> pageFetch(int offset) async {
    pageNumber++;
    print('page current: $pageNumber');
    var movies = await _repository.getPopularMovies(pageNumber);
    setPopularPageCounter(pageNumber);
    return pageNumber == 100 ? [] : movies.movies;
  }

  Future <List<Movie>> pageRefresh(int offset) async {
    pageNumber= 0;
    return pageFetch(offset);
  }

  dispose() {
    _moviesFetcher.close();
    _popularMoviesFetcher.close();
  }
}


