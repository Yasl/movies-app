import 'dart:async';
import 'movie_api_provider.dart';
import '../models/movie_model.dart';

class Repository {
  final moviesApiProvider = ParseMovies();

  Future<MovieModel> getMovies() => moviesApiProvider.fetchRandomMovies();
  Future<MovieModel> getPopularMovies(int page) => moviesApiProvider.fetchPopularMovies(page);

  int popularPageCounter;

  getPopularPageCounter (){
    return popularPageCounter = moviesApiProvider.popularPageCounter;
  }

  setPopularPageCounter (int page) {
    moviesApiProvider.popularPageCounter = page;
  }

  getMoviesWithYear(int year) {
    moviesApiProvider.fetchMoviesWithYear(year);
  }

  getMoviesWithGenre (String genre) {
    moviesApiProvider.fetchMoviesWithGenre(genre);
  }

  getMoviesWithYearAndGenre (int year, String genre) {
    moviesApiProvider.fetchMoviesWithYearAndGenre(genre, year);
  }

  }