import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../models/movie_model.dart';
import 'movie_genres.dart';
import 'dart:math';

class ParseMovies {
  final apiKey = 'bdb7c36020631076a58c1f8d52188e00';
  int popularPageCounter = 0;


  Future<MovieModel> fetchRandomMovies() async {
    var randomPage = Random().nextInt(100);
    var response = await http.get(
        "http://api.themoviedb.org/3/movie/popular?api_key=$apiKey&page=$randomPage");
//    print('response ${response.body}');
    if (response.statusCode == 200) {
      return MovieModel.fromJson(json.decode(response.body));
      // return Movie.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load');
    }
  }

  Future<MovieModel> fetchPopularMovies(int page) async {
    var response = await http.get(
            "https://api.themoviedb.org/3/discover/movie?api_key=$apiKey&page=$page&sort_by=popularity.desc");
    if (response.statusCode == 200) {
      return MovieModel.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load');
    }
  }

  Future<MovieModel> fetchMoviesWithYear (int year) async {
    var response = await http.get(
      "https://api.themoviedb.org/3/discover/movie?api_key=$apiKey&page=1&primary_release_year=$year");
    if (response.statusCode == 200) {
      return MovieModel.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load');
    }
  }

  Future<MovieModel> fetchMoviesWithGenre (String genre) async {
    var arrayInstance = GenresArray().genreMovieArray;

    var response = await http.get(
       ' https://api.themoviedb.org/3/discover/movie?api_key=bdb7c36020631076a58c1f8d52188e00&page=1&with_genres=${arrayInstance["$genre"]}');
    if (response.statusCode == 200) {
      return MovieModel.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load');
    }
  }

  Future<MovieModel> fetchMoviesWithYearAndGenre (String genre, int year) async {
    var arrayInstance = GenresArray().genreMovieArray;

    var response = await http.get(
        ' https://api.themoviedb.org/3/discover/movie?api_key=bdb7c36020631076a58c1f8d52188e00&page=1&primary_release_year=$year&with_genres=${arrayInstance["$genre"]}');
        if (response.statusCode == 200) {
      return MovieModel.fromJson(json.decode(response.body));
    } else {
    throw Exception('Failed to load');
    }
  }

}
