import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'movie_list.dart';
import 'random_film.dart';

class NavigationWidget extends StatefulWidget {
  @override
  _NavigationWidgetState createState() => _NavigationWidgetState();
}

class _NavigationWidgetState extends State<NavigationWidget> {
  int selectedIndex = 0;
  static const TextStyle optionStyle = TextStyle(fontSize: 24, fontWeight: FontWeight.bold);
  List<Widget> widgetOptions = <Widget>[

    RandomFilm(),
    MovieList(),
    Text(
      'Index 2: Saved films ',
      style: optionStyle,
    )
  ];

  void onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(child: widgetOptions.elementAt(selectedIndex)),
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.movie),
              title: Text('Random film'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.movie),
              title: Text('Popular'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.movie),
              title: Text('Saved'),
            ),
          ],
          currentIndex: selectedIndex,
          selectedItemColor: Colors.amber,
          onTap: onItemTapped,
        ));
  }
}
