import 'package:flutter/material.dart';
import '../models/movie_model.dart';
import '../blocs/movies_bloc.dart';
import 'movie_detail.dart';
import 'package:pagination_view/pagination_view.dart';

class MovieList extends StatefulWidget {
  @override
  _MovieListState createState() => _MovieListState();
}

class _MovieListState extends State<MovieList> {
  final bloc = MoviesBloc();
  PaginationViewType paginationViewType;
  GlobalKey<PaginationViewState> key;

  void initState() {
//    bloc.pageNumber = bloc.getPopularPageCounter();
    super.initState();
    bloc.getPopularMovies();
    paginationViewType = PaginationViewType.gridView;
    key = GlobalKey<PaginationViewState>();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Popular movies'),
      ),
      body: StreamBuilder(
        stream: bloc.popularMovies,
        builder: (context, AsyncSnapshot<MovieModel> snapshot) {
          if (snapshot.hasData) {
            snapshot.data.results.sort(
                (Movie a, Movie b) => a.popularity.compareTo(b.popularity));
            return buildList(snapshot);
          } else if (snapshot.hasError) {
            return Text(snapshot.error.toString());
          }
          return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }

  Widget buildList(AsyncSnapshot<MovieModel> snapshot) {
    return PaginationView<Movie>(
      key: key,
      preloadedItems: snapshot.data.movies,
      paginationViewType: paginationViewType,
      itemBuilder: (BuildContext context, Movie movie, int index) => GridTile(
          child: InkResponse(
        enableFeedback: true,
        child: Stack(alignment: Alignment.bottomLeft, children: <Widget>[
          Container(
            width: 350,
            height: 350,
            child: Container(
                child: movie.poster_path != null
                    ? Image.network(
                        'https://image.tmdb.org/t/p/w185${movie.poster_path}',
                        fit: BoxFit.cover)
                    : Container(
                        width: 100,
                        height: 100,
                        color: Colors.deepOrange,
                        child: Center(child: Icon(Icons.movie)))),
          ),
          Opacity(
              opacity: 0.8, // opacity работает?
              child: Container(
                  width: double.infinity,
                  height: 42,
                  decoration: BoxDecoration(color: Colors.black),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 4.0, top: 4.0),
                    child: Opacity(
                        opacity: 1, child: Text('${movie.title}', maxLines: 2)),
                  ))),
        ]),
        onTap: () => openDetailPage(snapshot.data.movies[index]),
      )),
      pageFetch: bloc.pageFetch,
      pageRefresh: bloc.pageRefresh,
      pullToRefresh: true,
      onError: (dynamic error) => Center(
        child: Text('Some error'),
      ),
      onEmpty: Center(child: Text('Sorry, List is empty')),
      bottomLoader: Center(
        child: CircularProgressIndicator(),
      ),
      initialLoader: Center(child: CircularProgressIndicator()),
    );
  }

  openDetailPage(Movie data) {
//    int currentPage = bloc
//        .getPopularPageCounter(); // Индекс пересчитывается из за подгрузки новых страниц, чтобы индекс всегда был от 0 до 19
//    index = index -
//        20 * (currentPage - 1); // (currentPage-1), т.к. счет начинается с 0.

    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) {
        return MovieDetail(
          title: data.title,
          posterUrl: data.backdrop_path,
          description: data.overview,
          releaseDate: data.release_date,
          voteAverage: data.vote_average.toString(),
          movieId: data.id,
        );
      }),
    );
  }

}
