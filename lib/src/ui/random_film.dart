import 'package:flutter/material.dart';
import '../models/movie_model.dart';
import '../blocs/movies_bloc.dart';
import '../resources/movie_genres.dart';
import 'dart:math';

class RandomFilm extends StatefulWidget {
  @override
  _RandomFilmState createState() => _RandomFilmState();
}

class _RandomFilmState extends State<RandomFilm> {
  final bloc = MoviesBloc();

  void initState() {
    super.initState();
    bloc.getMovies();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    bloc.getMovies();
    return Scaffold(
        appBar: AppBar(
          title: Text('Random movie'),
          actions: [
            IconButton(icon: Icon(Icons.filter_list), onPressed: filterDialog)
          ],
        ),
        body: StreamBuilder(
          stream: bloc.allMovies,
          builder: (context, AsyncSnapshot<MovieModel> snapshot) {
            if (snapshot.hasData) {
              int min = 1;
              int max = snapshot.data.results.length;
              Random rnd = Random();
              int r = min +
                  rnd.nextInt(
                      max - min); // get random index from movies list(0..20)
              return randomMovie(snapshot, r);
            } else if (snapshot.hasError) {
              return Text(snapshot.error.toString());
            }
            return Center(child: CircularProgressIndicator());
          },
        ));
  }

  Widget randomMovie(AsyncSnapshot<MovieModel> snapshot, int index) {
    return Column(children: [
      Expanded(
        child: Image.network(
          'https://image.tmdb.org/t/p/w500${snapshot.data.results[index]
              .poster_path}',
          fit: BoxFit.cover,
        ),
      ),
      Container(child: Text('${snapshot.data.results[index].title}')),
      RaisedButton(
          onPressed: () => nextMovie,
          child: const Text('Next movie', style: TextStyle(fontSize: 20))
      )
    ],
//      onTap: () => openDetailPage(snapshot.data, index),
    );
  }

  nextMovie (){
    if (bloc.yearDropDown != null) {
      print('called1');
      bloc.getMoviesWithYear();
    }
    else if (bloc.genreDropDown!= null) {
      print('called2');
      bloc.getMoviesWithGenre();
    }
    else if (bloc.yearDropDown!= null || bloc.genreDropDown!= null) {
      print('called3');
      bloc.getMoviesWithYearAndGenre();
    }
    else {
      print('called');
    bloc.getMovies();
    }
    setState(() {

    });
  }

  filterDialog() {
    return showDialog<void>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Фильтр страниц'),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  Text('По году релиза:'),
                  filterYearSelect(),
                  Text('Выбрать жанры:'),
                  filterGenreSelect()
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(child: Text('Cancel'), onPressed: () {}),
              FlatButton(child: Text('Confirm'), onPressed: () {}),
            ],
          );
        });
  }

  filterYearSelect() {
    var dropdownValue = DateTime.now().year.toInt();

    var yearArray = List<int>();
    for (int i = 0, x = 1970; x <= dropdownValue; i++, x++) {
      yearArray.add(x);
    }
    yearArray.sort((int a, int b) => b.compareTo(a));
    return DropdownButton<int>(
      value: dropdownValue,
      elevation: 16,
      style: TextStyle(color: Colors.amber),
      underline: Container(
        height: 2,
        color: Colors.amber,
      ),
      onChanged: (int newValue) {
        setState(() {
          dropdownValue = newValue;
        });
      },
      items: yearArray.map<DropdownMenuItem<int>>((int value) {
        return DropdownMenuItem<int>(
          value: value,
          child: Text('$value'),
        );
      }).toList(),
    );

  }

  filterGenreSelect() {
    var dropdownValue = 'Action';
    var currentGenreArray = GenresArray().genreMovieArray;

    return DropdownButton<String>(
      value: dropdownValue,
      elevation: 16,
      style: TextStyle(color: Colors.amber),
      underline: Container(
        height: 2,
        color: Colors.amber,
      ),
      onChanged: (String newValue) {
        setState(() {
          dropdownValue = newValue;
        });
      },
      items: currentGenreArray.map((name, value) {
        return MapEntry(
          name,
          DropdownMenuItem<String>(
            value: name,
            child: Text(name),
          )
        );
      })
        .values
        .toList(),


//      currentGenreArray.map<DropdownMenuItem<String,int>>((int key,String value) {
//        return DropdownMenuItem<String,int>(
//          key: key,
//          value: value,
//          child: Text('$key'),
//        );
//      }).toList(),
    );
  }

}

