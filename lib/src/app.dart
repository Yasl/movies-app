import 'package:flutter/material.dart';
import 'ui/Navigation.dart';
//import 'ui/movie_list.dart';

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Movies app',
      theme: ThemeData.dark(),
      home: Scaffold(
        body: NavigationWidget(),
      ),
    );
  }
}