//import 'dart:convert';

class MovieModel {
  int currentPage;
  int TotalResults;
  int TotalPages;
  List<Movie> movies = [];

  MovieModel.fromJson(Map<String, dynamic> parsedJson) {
    currentPage = parsedJson['page'];
    TotalResults = parsedJson['total_results'];
    TotalPages = parsedJson['total_pages'];
    List<Movie> tmp = []; //обязательно ли?
    for (int i = 0; i < parsedJson['results'].length; i++) {
      Movie newMovie = Movie.fromJson(parsedJson['results'][i]);
      tmp.add(newMovie);
    }
    movies = tmp;
//    print(movies);
  }

  List<Movie> get results => movies;
}

class Movie {
  double popularity;
  int id;
  bool video;
  int vote_count;
  var vote_average;
  String title;
  DateTime release_date; //!!!! format?
  String original_language;
  String original_title;
  List genre_ids = <int>[];
  String backdrop_path;
  bool adult;
  String overview;
  String poster_path;

  Movie.fromJson(parsedJson){
    popularity = parsedJson['popularity'];
    id = parsedJson['id'];
    video = parsedJson['video'];
    vote_count = parsedJson['vote_count'];
    vote_average = parsedJson['vote_average'];
    title = parsedJson['title'];
    release_date = DateTime.parse(parsedJson['release_date']); //!!!! format?
    original_language = parsedJson['original_language'];
    original_title = parsedJson['original_title'];
    for (int i = 0; i < parsedJson['genre_ids'].length; i++) {
      genre_ids.add(parsedJson['genre_ids'][i]);
    }
    backdrop_path = parsedJson['backdrop_path'];
    adult = parsedJson['adult'];
    overview = parsedJson['overview'];
    poster_path = parsedJson['poster_path'];
  }

  Movie(result) {
    popularity = result['popularity'];
    id = result['id'];
    video = result['video'];
    vote_count = result['vote_count'];
    vote_average = result['vote_average'];
    title = result['title'];
    release_date = result['release_date']; //!!!! format?
    original_language = result['original_language'];
    original_title = result['original_title'];
    for (int i = 0; i < result['genre_ids'].length; i++) {
      genre_ids.add(result['genre_ids'][i]);
    }
    backdrop_path = result['backdrop_path'];
    adult = result['adult'];
    overview = result['overview'];
    poster_path = result['poster_path'];
  }
}
